import requests
from prefect import task, Flow, Parameter
from prefect.tasks.postgres.postgres import PostgresExecute, PostgresFetch
import googlesearch
import itertools

@task
def extract_google(request):
    return googlesearch.search(request)


@task
def extract_yandex(request):
    r = requests.get('https://yandex.ru/search', params={'text': request},
                     headers={'Host': 'yandex.ru',
                              'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0',
                              'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                              'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                              'Accept-Encoding': 'gzip, deflate, br',
                              'Connection': 'keep-alive',
                              'Cookie': 'yandexuid=7491000651536427101; i=YIemWp87NArtn0WADj0Ri82jQT9noiM0/ZmaKuykVOBb3puW/omrJNP4FC6h8KMpnLWKA20ON6WvflMvUM5gy1DOnRM=; yp=1615990044.ygu.1#1851788777.udn.cDpzaHVtYWNrb3Y3Mzk5#1598132627.stltp.serp_bk-map_1_1566596627#1585035425.szm.1:1920x1080:1853x951; _ym_uid=1536427104115943757; mda=0; yandex_gid=2; yabs-frequency=/4/4G0N01AbSbxDazfT/c4m-RKWw8DjBi718EY00/; my=YwA=; fuid01=5b940ad16af84225.LgBg6ugL6F0iEx4u6dwyqi0ngc1C4PUtyOqpDdynGzEXUCx_a4Otmu45v18W2gzFqUIckTrMa7rkIh_KUJe73EWQqv1vZBnxKEDebcjxWhwdXK6_oAaEeBGkcrpACcF2; Session_id=3:1584430624.5.0.1536428777904:IBwqXA:84.1|142099036.0.2|214091.403378.9p89VNvCn6HYZ4PLFM4EZdCBOyY; sessionid2=3:1584430624.5.0.1536428777904:IBwqXA:84.1|142099036.0.2|214091.832259.5jkmvjchaR5HlbjnVTarZf8caUQ; L=ZgwDW0gMAWZ/cGNkQ1dZfUVZU0tiYAQNHh0tKQIHXDUye2NSdw==.1536428777.13616.39105.bc204732fe19a43da462caf9f8e57cbe; yandex_login=shumackov7399; font_loaded=YSv1; _ym_uid=1536427104115943757; _ym_d=1584440182; gdpr=0; ymex=1898602735.yrts.1583242735; zm=m-white_bender.webp.css-https%3As3home-static_k6S7IjoBbCK05jwm1xDstLiuf-s%3Al; _ym_isad=2; ys=wprid.1584571665828762-275638558544976883400128-vla1-4219; _csrf=MEve5j7DgI7ps9-U2URCG4V2',
                              'Upgrade-Insecure-Requests': '1',
                              'Cache-Control': 'max-age=0',
                              'TE': 'Trailers'})
    return r.text


@task
def transform_data(google_data, yandex_data):
    list_of_google_data = [url[1] for url in itertools.takewhile(lambda i: i[0] < 20, enumerate(google_data))]
    list_of_yandex_data = yandex_data[yandex_data.find('Favicon-Page0') +
                                      len('Favicon-Page0.Favicon-Icon{background-image:url(//favicon.yandex.net/favicon/v2/'):yandex_data.find('yandex.ru?size')]
    list_of_yandex_data = list_of_yandex_data.split(';')[:-1]
    intersection = set(list_of_google_data) & set(list_of_yandex_data)
    return list_of_google_data, list_of_yandex_data, intersection


@task
def create_tables():
    insert = PostgresExecute(db_name='requests', user='test_user', password='qazedcwsx', host='localhost')
    select = PostgresFetch(db_name='requests', user='test_user', password='qazedcwsx', host='localhost')
    tables = select.run(fetch='all', query='SELECT * FROM pg_catalog.pg_tables')
    tables = {row[1] for row in tables}
    if 'intersection' in tables:
        insert.run(query='delete from intersection')
    else:
        insert.run(query='create table intersection (id int primary key, url text)')
    if 'google_urls' in tables:
        insert.run(query='delete from google_urls')
    else:
        insert.run(query='create table google_urls (id int primary key, url text)')
    if 'yandex_urls' in tables:
        insert.run(query='delete from yandex_urls')
    else:
        insert.run(query='create table yandex_urls (id int primary key, url text)')


@task
def load_data(google_list, yandex_list, intersection):
    insert = PostgresExecute(db_name='requests', user='test_user', password='qazedcwsx', host='localhost')
    print("Database opened successfully")
    for i, url in enumerate(google_list):
        insert.run(query="insert into google_urls (id, url) values (%s, %s)", data=(i, url))
    for i, url in enumerate(yandex_list):
        insert.run(query="insert into yandex_urls (id, url) values (%s, %s)", data=(i, url))
    for i, url in enumerate(intersection):
        insert.run(query="insert into intersection (id, url) values (%s, %s)", data=(i, url))
    print("Record inserted successfully")


@task
def print_records():
    select = PostgresFetch(db_name='requests', user='test_user', password='qazedcwsx', host='localhost')
    records_g = select.run(fetch='all', query='SELECT * from google_urls')
    records_y = select.run(fetch='all', query='SELECT * from yandex_urls')
    records_i = select.run(fetch='all', query='SELECT * from intersection')
    print('google:')
    for row in records_g:
        print("url:", row[1])
    print('yandex:')
    for row in records_y:
        print("url:", row[1])
    print('intersection')
    for row in records_i:
        print("url:", row[1])


with Flow('browser-etl') as flow:
    request = Parameter('request')
    google_data = extract_google(request)
    yandex_data = extract_yandex(request)
    transformed = transform_data(google_data, yandex_data, upstream_tasks=[google_data, yandex_data])
    create = create_tables()
    load = load_data(transformed[0], transformed[1], transformed[2], upstream_tasks=[transformed])
    print_rec = print_records(upstream_tasks=[load])
s = input('request: ')
flow.run(request=s)